
# IssueTracker

## setup
- npm install
- ng test

## Entrypoint
- the app's entrypoint is at /src/app/app.component.ts
- the test files are the .spec ones


## Structure

- there are 1 page, where I imagine a form or something, where you can submit the bug/problem. We should restrict here when the user can submit a bug.
- if you can submit a bug, the ap uses the dueDateCalculatorService, which relies on 2 services
- DateService is responsible for basic date commands
- WorkingHoursService is responsible for calculations based on the company's working hours

## CalculateDueDate

### the main steps to get the dueDate:

- can I start it now? If not when is the next, when I can start?
- how many "days" would it take if there would be no weekends?
- how many weekends do we have between the dates w start and finish? Extend the dueDate.

The main reason is that if there would be holidays or etc, we just have to extend the dueDate
