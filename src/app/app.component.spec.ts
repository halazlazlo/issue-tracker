import { AppComponent } from './app.component';
import { Bug } from './models/bug.model';
import { DateService } from './services/date.service';
import { DateServiceInterface } from './services/date.service.interface';
import { DueDateCalculatorService } from './services/due-date-calculator.service';
import { DueDateCalculatorServiceInterface } from './services/due-date-calculator.service.interface';
import { WorkingHoursService } from './services/working-hours.service';
import { WorkingHoursServiceInterface } from './services/working-hours.service.interface';

describe('App', () => {
  let workingHoursService: WorkingHoursServiceInterface;
  let dateService: DateServiceInterface;
  let dueDateCalculatorService: DueDateCalculatorServiceInterface;

  let app: AppComponent;

  beforeEach(() => {
    workingHoursService = new WorkingHoursService();
    dateService = new DateService();

    dueDateCalculatorService = new DueDateCalculatorService(
      workingHoursService,
      dateService
    );

    app = new AppComponent(
      dateService,
      workingHoursService,
      dueDateCalculatorService
    );
  });

  it('should not allow submitting before working hours', () => {
    expect(() => {
      app.submit(new Bug({
        submittedAt: new Date(2018, 5, 18, 8, 59),
        turnaroundTimeInHours: 1
      }));
    }).toThrow();
  });

  it('should not allow submitting after working hours', () => {
    expect(() => {
      app.submit(new Bug({
        submittedAt: new Date(2018, 5, 18, 17, 1),
        turnaroundTimeInHours: 1
      }));
    }).toThrow();
  });

  it('should not allow submitting on weekends', () => {
    expect(() => {
      app.submit(new Bug({
        submittedAt: new Date(2018, 5, 23, 10, 0),
        turnaroundTimeInHours: 1
      }));
    }).toThrow();
  });
});
