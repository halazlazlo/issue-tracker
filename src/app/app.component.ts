import { Component } from '@angular/core';
import { Bug } from './models/bug.model';
import { DateServiceInterface } from './services/date.service.interface';
import { DueDateCalculatorServiceInterface } from './services/due-date-calculator.service.interface';
import { WorkingHoursServiceInterface } from './services/working-hours.service.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(
    private dateService: DateServiceInterface,
    private workingHoursService: WorkingHoursServiceInterface,
    private dueDateCalculatorService: DueDateCalculatorServiceInterface
  ) {}

  submit(bug: Bug) {
    if (
      this.workingHoursService.isDateOutsideWorkingHours(bug.submittedAt) ||
      this.dateService.isWeekendDay(bug.submittedAt)
    ) {
      throw new Error('Invalid argument: submitted at');
    }

    return this.dueDateCalculatorService.calculateDueDate(bug.submittedAt, bug.turnaroundTimeInHours);
  }
}
