import { WorkingHoursServiceInterface } from './working-hours.service.interface';

export class WorkingHoursService implements WorkingHoursServiceInterface {
  private workingHoursStart = 9;
  private workingHoursEnd = 17;

  isDateBetweenWorkingHours(date: Date): boolean {
    return !this.isDateBeforeWorkingHours(date) && !this.isDateAfterWorkingHours(date);
  }

  isDateOutsideWorkingHours(date: Date): boolean {
    return !this.isDateBetweenWorkingHours(date);
  }

  isDateBeforeWorkingHours(date: Date): boolean {
    return date.getHours() < this.workingHoursStart;
  }

  isDateAfterWorkingHours(date: Date): boolean {
    const hour = date.getHours();

    return hour > this.workingHoursEnd || (hour === this.workingHoursEnd && date.getMinutes() > 0);
  }

  getWorkingHoursLength(): number {
    return this.workingHoursEnd - this.workingHoursStart;
  }

  getOutsideWorkingHoursLength(): number {
    return 24 - (this.workingHoursEnd - this.workingHoursStart);
  }

  getHoursLengthUntilWorkingHours(date: Date): number {
    let hoursUntilWorkingHours = 0;

    if (this.isDateBeforeWorkingHours(date)) {
      hoursUntilWorkingHours = this.workingHoursStart - date.getHours();
    } else {
      hoursUntilWorkingHours = 24 - date.getHours() + this.workingHoursStart;
    }

    return hoursUntilWorkingHours;
  }
}
