import { DateService } from './date.service';
import { DateServiceInterface } from './date.service.interface';
import { DueDateCalculatorService } from './due-date-calculator.service';
import { DueDateCalculatorServiceInterface } from './due-date-calculator.service.interface';
import { WorkingHoursService } from './working-hours.service';
import { WorkingHoursServiceInterface } from './working-hours.service.interface';

describe('DueDateCalculator', () => {
  let dateService: DateServiceInterface;
  let workingHoursService: WorkingHoursServiceInterface;
  let dueDateCalculator: DueDateCalculatorServiceInterface;

  beforeEach(() => {
    dateService = new DateService();
    workingHoursService = new WorkingHoursService();
    dueDateCalculator = new DueDateCalculatorService(workingHoursService, dateService);
  });

  describe('Should recieve valid arguments', () => {
    it('turnaroundTime should be > 0', () => {
      expect(() => {
        dueDateCalculator.calculateDueDate(new Date(), 0);
      }).toThrow();

      expect(() => {
        dueDateCalculator.calculateDueDate(new Date(), -1);
      }).toThrow();
    });
  });

  describe('Should calculate if the range is within working hours', () => {
    it('range is part of working hours', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 9, 12),
        1
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 18, 10, 12).getTime());
    });

    it('range is the full day', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 9, 0),
        8
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 18, 17, 0).getTime());
    });
  });

  describe('Consider working hours', () => {
    it('due date is after close time', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 16, 12),
        1
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 19, 9, 12).getTime());
    });

    it('due date ends after close time and turnaroundTime is 1 day', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 9, 12),
        8
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 19, 9, 12).getTime());
    });

    it('due date ends before open time', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 16, 0),
        9
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 19, 17, 0).getTime());
    });

    it('If a problem was reported at 2:12PM on Tuesday then it is due by 2:12PM on Thursday', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 19, 14, 12),
        16
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 21, 14, 12).getTime());
    });

    it('turnaround time is more than 2 working day', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 16, 0),
        18
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 21, 10, 0).getTime());
    });
  });

  describe('Consider weekends', () => {
    it('should watch weekends', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 22, 9, 0),
        16
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 25, 17, 0).getTime());
    });

    it('should handle if due date comes to a saturday', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 22, 16, 0),
        2
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 25, 10, 0).getTime());
    });

    it('should handle if is submitted on a saturday', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 23, 9, 0),
        1
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 25, 10, 0).getTime());
    });

    it('should handle if is submitted on a sunday', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 24, 9, 0),
        1
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 25, 10, 0).getTime());
    });
  });

  describe('Bonus: Consider submitting outside working hours', () => {
    it('submitted before working hours start', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 18, 8, 0),
        1
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 18, 10, 0).getTime());
    });

    it('submitted after working hours end', () => {
      const dueDate = dueDateCalculator.calculateDueDate(
        new Date(2018, 5, 17, 18, 0),
        1
      );

      expect(dueDate.getTime()).toBe(new Date(2018, 5, 18, 10, 0).getTime());
    });
  });
});
