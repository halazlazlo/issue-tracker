export interface DueDateCalculatorServiceInterface {
  calculateDueDate(submittedAt: Date, turnaroundTimeInHours: number): Date;
}
