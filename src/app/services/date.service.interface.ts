export interface DateServiceInterface {
  addHours(date: Date, hours: number): Date;
  addDays(date: Date, days: number): Date;

  countWeekendsInRange(from: Date, to: Date): number;

  isSaturday(date: Date): boolean;
  isWeekendDay(date: Date): boolean;
}
