import { DateService } from './date.service';
import { DateServiceInterface } from './date.service.interface';

describe('DateService', () => {
  let dateService: DateServiceInterface;

  beforeEach(() => {
    dateService = new DateService();
  });

  it('should add hours', () => {
    const date = new Date(2017, 11, 31, 23, 21);
    const newDate = dateService.addHours(date, 1);

    expect(newDate.getTime()).toBe(new Date(2018, 0, 1, 0, 21).getTime());
  });

  it('should add days');
  it('should countWeekendsInRange');
  it('should check if a date is a saturday');
  it('should check if a date is a weekend day');
});
