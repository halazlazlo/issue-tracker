import { DateServiceInterface } from './date.service.interface';

export class DateService implements DateServiceInterface {
  addHours(date: Date, hours: number): Date {
    if (date && hours > 0) {
      date.setHours(date.getHours() + hours);
    }

    return date;
  }

  addDays(date: Date, days: number): Date {
    if (days > 0) {
      date.setDate(date.getDate() + days);
    }

    return date;
  }

  countWeekendsInRange(from: Date, to: Date): number {
    let weekendsCount = 0;

    let current = new Date(from.getFullYear(), from.getMonth(), from.getDate());
    to = new Date(to.getFullYear(), to.getMonth(), to.getDate());

    while (current <= to) {
      if (this.isWeekendDay(current)) {
        weekendsCount++;
      }

      current = this.addDays(current, 1);
    }

    return weekendsCount;
  }

  isSaturday(date: Date): boolean {
    return date.getDay() === 6;
  }

  isWeekendDay(date: Date): boolean {
    const day = date.getDay();

    return day === 6 || day === 0;
  }
}
