export interface WorkingHoursServiceInterface {
  isDateBetweenWorkingHours(date: Date): boolean;
  isDateOutsideWorkingHours(date: Date): boolean;

  isDateBeforeWorkingHours(date: Date): boolean;
  isDateAfterWorkingHours(date: Date): boolean;

  getWorkingHoursLength(): number;
  getOutsideWorkingHoursLength(): number;

  getHoursLengthUntilWorkingHours(date: Date): number;
}
