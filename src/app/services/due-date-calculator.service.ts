import { DateServiceInterface } from './date.service.interface';
import { DueDateCalculatorServiceInterface } from './due-date-calculator.service.interface';
import { WorkingHoursServiceInterface } from './working-hours.service.interface';

export class DueDateCalculatorService implements DueDateCalculatorServiceInterface {
  private turnaroundTimeInHours: number;
  private submittedAt: Date;
  private dueDate: Date;

  constructor(
    private workingHoursService: WorkingHoursServiceInterface,
    private dateService: DateServiceInterface
  ) {}

  calculateDueDate(submittedAt: Date, turnaroundTimeInHours: number): Date {
    if (turnaroundTimeInHours < 1) {
      throw new Error('turnaroundTimeInHours should be greater than 0');
    }

    this.turnaroundTimeInHours = turnaroundTimeInHours;
    this.submittedAt = submittedAt;

    this.initStartDate();

    this.addWorkingHours();

    this.addOutsideWorkingHours();

    this.addWeekends();

    return this.dueDate;
  }

  private initStartDate() {
    if (this.workingHoursService.isDateOutsideWorkingHours(this.submittedAt)) {
      this.dueDate = this.dateService.addHours(
        this.submittedAt,
        this.workingHoursService.getHoursLengthUntilWorkingHours(this.submittedAt)
      );
    } else {
      this.dueDate = this.submittedAt;
    }
  }

  private addWorkingHours() {
    const workingHoursLength = this.workingHoursService.getWorkingHoursLength();
    let days = Math.floor(this.turnaroundTimeInHours / workingHoursLength);
    let hours = this.turnaroundTimeInHours % workingHoursLength;

    if (hours === 0) {
      days--;
      hours = workingHoursLength;
    }

    hours += days * 24;

    this.dueDate = this.dateService.addHours(
      this.dueDate,
      hours
    );
  }

  private addOutsideWorkingHours() {
    if (this.workingHoursService.isDateOutsideWorkingHours(this.dueDate)) {
      this.dueDate = this.dateService.addHours(
        this.dueDate,
        this.workingHoursService.getOutsideWorkingHoursLength()
      );
    }
  }

  private addWeekends() {
    let weekendsCount = this.dateService.countWeekendsInRange(this.submittedAt, this.dueDate);

    if (this.dateService.isSaturday(this.dueDate)) {
      weekendsCount++;
    }

    this.dueDate = this.dateService.addDays(
      this.dueDate,
      weekendsCount
    );
  }
}
