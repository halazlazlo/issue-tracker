export class Bug {
  submittedAt: Date;
  turnaroundTimeInHours: number;

  constructor(init: {
    submittedAt: Date;
    turnaroundTimeInHours: number;
  }) {
    this.submittedAt = init.submittedAt;
    this.turnaroundTimeInHours = init.turnaroundTimeInHours;
  }
}
